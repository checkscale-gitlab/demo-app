# http-client

This example request can be executed with the http-client:

- [HTTP client in IntelliJ IDEA code editor](https://www.jetbrains.com/help/idea/http-client-in-product-code-editor.html)
- [VSCode REST Client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client)
